external_url 'https://gitlab.example.com'

git_data_dirs({
  'local' => { 'path' => '/tmp/git-data' }
})

roles(['application_role', 'sidekiq_role'])
alertmanager['enable'] = false
consul['enable'] = false
gitaly['enable'] = false
gitlab_exporter['enable'] = false
gitlab_kas['enable'] = false
gitlab_workhorse['enable'] = true
grafana['enable'] = false
letsencrypt['enable'] = false
nginx['enable'] = true
postgresql['enable'] = false
praefect['enable'] = false
prometheus['enable'] = false
puma['enable'] = true
redis['enable'] = false
sidekiq['enable'] = true
logrotate['enable'] = true

gitlab_rails['db_port'] = 5432
gitlab_rails['db_user'] = 'postgres'
gitlab_rails['db_password'] = 'password'
gitlab_rails['db_host'] = '172.17.0.3'
gitlab_rails['auto_migrate'] = false

gitlab_rails['redis_host'] = '172.17.0.4'
gitlab_rails['redis_port'] = 6379

node_exporter['listen_address'] = '0.0.0.0:9100'
sidekiq['listen_address'] = "0.0.0.0"
puma['listen'] = '0.0.0.0'

gitlab_rails['monitoring_whitelist'] = ['10.6.0.81/32', '127.0.0.0/8']
nginx['status']['options']['allow'] = ['10.6.0.81/32', '127.0.0.0/8']
nginx['listen_addresses'] = ["0.0.0.0", "[::]"]

gitlab_rails['object_store']['enabled'] = false

