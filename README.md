# THIS SETUP WILL NOT WORK!

This setup is to provide an example for a couple problem, that I have with setting up a GitLab HA installation.

## Starting the setup

- `docker-compose up -d`
- optionally checking the logs in a new terminal: `docker-compose logs gitlab`
- *wait for the gitlab container to initialize, might need a second start*
- Change the root password: `docker-compose exec gitlab gitlab-rake gitlab:password:reset[root]`

## The problems (and how to get there)

### `uninitialized constant Gitlab::Redis::ALL_CLASSES`

Just run a `docker-compose up`. It will come on it's own

#### Details

The full docker compose output related to this error:

```
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31248 {"timestamp":"2023-09-25T20:09:11.312Z","pid":662,"message":"! Unable to load application: NameError: uninitialized constant Gitlab::Redis::ALL_CLASSES"}
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31252 bundler: failed to load command: puma (/opt/gitlab/embedded/bin/puma)
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31268 /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/instrumentation/redis.rb:12:in `<class:Redis>': uninitialized constant Gitlab::Redis::ALL_CLASSES (NameError)
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31269     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/instrumentation/redis.rb:6:in `<module:Instrumentation>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31270     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/instrumentation/redis.rb:4:in `<module:Gitlab>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31270     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/instrumentation/redis.rb:3:in `<top (required)>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31271     from <internal:/opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/rubygems/core_ext/kernel_require.rb>:38:in `require'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31271     from <internal:/opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/rubygems/core_ext/kernel_require.rb>:38:in `require'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31272     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/zeitwerk-2.6.7/lib/zeitwerk/kernel.rb:30:in `require'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31272     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/inflector/methods.rb:278:in `const_get'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31273     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/inflector/methods.rb:278:in `constantize'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31273     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/core_ext/string/inflections.rb:74:in `constantize'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31274     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/redis/wrapper.rb:83:in `instrumentation_class'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31276     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/redis/wrapper.rb:125:in `redis_store_options'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31277     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/redis/wrapper.rb:98:in `params'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31277     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/redis/wrapper.rb:22:in `params'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31277     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/redis/wrapper.rb:89:in `redis'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31278     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/redis/wrapper.rb:33:in `block in pool'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31278     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/connection_pool-2.3.0/lib/connection_pool/timed_stack.rb:169:in `try_create'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31279     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/connection_pool-2.3.0/lib/connection_pool/timed_stack.rb:73:in `block (2 levels) in pop'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31279     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/connection_pool-2.3.0/lib/connection_pool/timed_stack.rb:69:in `loop'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31280     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/connection_pool-2.3.0/lib/connection_pool/timed_stack.rb:69:in `block in pop'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31280     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/connection_pool-2.3.0/lib/connection_pool/timed_stack.rb:68:in `synchronize'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31281     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/connection_pool-2.3.0/lib/connection_pool/timed_stack.rb:68:in `pop'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31281     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/connection_pool-2.3.0/lib/connection_pool.rb:80:in `checkout'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31282     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/connection_pool-2.3.0/lib/connection_pool.rb:62:in `block in with'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31283     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/connection_pool-2.3.0/lib/connection_pool.rb:61:in `handle_interrupt'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31283     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/connection_pool-2.3.0/lib/connection_pool.rb:61:in `with'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31284     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache/redis_cache_store.rb:334:in `block in read_serialized_entry'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31284     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache/redis_cache_store.rb:466:in `failsafe'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31284     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache/redis_cache_store.rb:333:in `read_serialized_entry'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31285     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache/strategy/local_cache.rb:118:in `read_serialized_entry'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31286     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache/redis_cache_store.rb:329:in `read_entry'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31286     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache.rb:328:in `block in fetch'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31287     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache.rb:783:in `block in instrument'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31287     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/notifications.rb:208:in `instrument'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31287     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache.rb:783:in `instrument'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31288     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache.rb:327:in `fetch'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31289     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/flipper-active_support_cache_store-0.26.2/lib/flipper/adapters/active_support_cache_store.rb:129:in `read_feature_keys'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31289     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/flipper-active_support_cache_store-0.26.2/lib/flipper/adapters/active_support_cache_store.rb:39:in `features'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31290     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/flipper-active_support_cache_store-0.26.2/lib/flipper/adapters/active_support_cache_store.rb:129:in `block in read_feature_keys'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31291     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache.rb:809:in `block in save_block_result_to_cache'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31291     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache.rb:783:in `block in instrument'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31292     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/notifications.rb:208:in `instrument'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31292     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache.rb:783:in `instrument'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31293     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache.rb:808:in `save_block_result_to_cache'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31293     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/cache.rb:338:in `fetch'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31294     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/flipper-active_support_cache_store-0.26.2/lib/flipper/adapters/active_support_cache_store.rb:129:in `read_feature_keys'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31294     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/flipper-active_support_cache_store-0.26.2/lib/flipper/adapters/active_support_cache_store.rb:39:in `features'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31295     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/flipper-0.26.2/lib/flipper/adapters/memoizable.rb:42:in `features'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31295     from /opt/gitlab/embedded/service/gitlab-rails/lib/feature.rb:62:in `persisted_names'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31296     from /opt/gitlab/embedded/service/gitlab-rails/lib/feature.rb:69:in `persisted_name?'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31296     from /opt/gitlab/embedded/service/gitlab-rails/lib/feature.rb:234:in `block in current_feature_value'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31296     from /opt/gitlab/embedded/service/gitlab-rails/lib/feature.rb:252:in `with_feature'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31297     from /opt/gitlab/embedded/service/gitlab-rails/lib/feature.rb:233:in `current_feature_value'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31298     from /opt/gitlab/embedded/service/gitlab-rails/lib/feature.rb:88:in `enabled?'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31298     from /opt/gitlab/embedded/service/gitlab-rails/app/models/application_setting.rb:994:in `should_prevent_visibility_restriction?'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31299     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:400:in `block in make_lambda'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31299     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:179:in `block (2 levels) in halting_and_conditional'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31299     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:179:in `all?'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31300     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:179:in `block in halting_and_conditional'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31300     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:595:in `block in invoke_before'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31301     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:595:in `each'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31302     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:595:in `invoke_before'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31302     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:106:in `run_callbacks'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31303     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:929:in `_run_validate_callbacks'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31303     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activemodel-7.0.6/lib/active_model/validations.rb:406:in `run_validations!'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31304     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activemodel-7.0.6/lib/active_model/validations/callbacks.rb:115:in `block in run_validations!'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31304     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:107:in `run_callbacks'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31305     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/callbacks.rb:929:in `_run_validation_callbacks'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31306     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activemodel-7.0.6/lib/active_model/validations/callbacks.rb:115:in `run_validations!'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31306     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activemodel-7.0.6/lib/active_model/validations.rb:337:in `valid?'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31307     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/validations.rb:68:in `valid?'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31307     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/validations.rb:84:in `perform_validations'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31308     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/validations.rb:47:in `save'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31308     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/transactions.rb:298:in `block in save'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31309     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/transactions.rb:354:in `block in with_transaction_returning_status'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31309     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/connection_adapters/abstract/database_statements.rb:314:in `transaction'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31310     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/transactions.rb:350:in `with_transaction_returning_status'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31310     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/transactions.rb:298:in `save'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31311     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/suppressor.rb:50:in `save'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31311     from <internal:kernel>:90:in `tap'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31312     from /opt/gitlab/embedded/service/gitlab-rails/app/models/application_setting_implementation.rb:299:in `create_from_defaults'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31312     from /opt/gitlab/embedded/service/gitlab-rails/app/models/application_setting.rb:897:in `block in create_from_defaults'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31313     from /opt/gitlab/embedded/service/gitlab-rails/app/models/concerns/cross_database_modification.rb:92:in `block in transaction'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31313     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/connection_adapters/abstract/transaction.rb:319:in `block in within_new_transaction'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31314     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/concurrency/load_interlock_aware_monitor.rb:25:in `handle_interrupt'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31314     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/concurrency/load_interlock_aware_monitor.rb:25:in `block in synchronize'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31315     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/concurrency/load_interlock_aware_monitor.rb:21:in `handle_interrupt'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31315     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/concurrency/load_interlock_aware_monitor.rb:21:in `synchronize'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31316     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/connection_adapters/abstract/transaction.rb:317:in `within_new_transaction'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31316     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/connection_adapters/abstract/database_statements.rb:316:in `transaction'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31318     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activerecord-7.0.6/lib/active_record/transactions.rb:209:in `transaction'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31318     from /opt/gitlab/embedded/service/gitlab-rails/app/models/concerns/cross_database_modification.rb:83:in `transaction'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31319     from /opt/gitlab/embedded/service/gitlab-rails/app/models/application_setting.rb:896:in `create_from_defaults'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31319     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/current_settings.rb:71:in `uncached_application_settings'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31320     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/current_settings.rb:42:in `ensure_application_settings!'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31320     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/current_settings.rb:15:in `block in current_application_settings'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31321     from /opt/gitlab/embedded/service/gitlab-rails/gems/gitlab-safe_request_store/lib/gitlab/safe_request_store/null_store.rb:37:in `fetch'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31321     from /opt/gitlab/embedded/lib/ruby/3.0.0/forwardable.rb:238:in `fetch'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31322     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/current_settings.rb:15:in `current_application_settings'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31322     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/current_settings.rb:32:in `method_missing'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31323     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/metrics/prometheus.rb:99:in `prometheus_metrics_enabled_unmemoized'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31323     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/metrics/prometheus.rb:24:in `block in prometheus_metrics_enabled?'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31324     from /opt/gitlab/embedded/service/gitlab-rails/gems/gitlab-utils/lib/gitlab/utils/strong_memoize.rb:34:in `strong_memoize'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31324     from /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/metrics/prometheus.rb:23:in `prometheus_metrics_enabled?'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31325     from /opt/gitlab/embedded/service/gitlab-rails/config/initializers/7_prometheus_metrics.rb:59:in `<top (required)>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31325     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/engine.rb:667:in `load'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31326     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/engine.rb:667:in `block in load_config_initializer'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31326     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/activesupport-7.0.6/lib/active_support/notifications.rb:208:in `instrument'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31327     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/engine.rb:666:in `load_config_initializer'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31327     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/engine.rb:620:in `block (2 levels) in <class:Engine>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31328     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/engine.rb:619:in `each'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31328     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/engine.rb:619:in `block in <class:Engine>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31329     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/initializable.rb:32:in `instance_exec'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31329     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/initializable.rb:32:in `run'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31329     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/initializable.rb:61:in `block in run_initializers'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31330     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:228:in `block in tsort_each'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31330     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:350:in `block (2 levels) in each_strongly_connected_component'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31331     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:422:in `block (2 levels) in each_strongly_connected_component_from'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31332     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:431:in `each_strongly_connected_component_from'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31333     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:421:in `block in each_strongly_connected_component_from'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31333     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/initializable.rb:50:in `each'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31333     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/initializable.rb:50:in `tsort_each_child'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31334     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:415:in `call'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31334     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:415:in `each_strongly_connected_component_from'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31335     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:349:in `block in each_strongly_connected_component'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31335     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:347:in `each'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31336     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:347:in `call'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31336     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:347:in `each_strongly_connected_component'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31337     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:226:in `tsort_each'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31338     from /opt/gitlab/embedded/lib/ruby/3.0.0/tsort.rb:205:in `tsort_each'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31338     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/initializable.rb:60:in `run_initializers'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31338     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/railties-7.0.6/lib/rails/application.rb:372:in `initialize!'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31339     from /opt/gitlab/embedded/service/gitlab-rails/config/environment.rb:7:in `<top (required)>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31339     from <internal:/opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/rubygems/core_ext/kernel_require.rb>:38:in `require'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31340     from <internal:/opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/rubygems/core_ext/kernel_require.rb>:38:in `require'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31340     from /opt/gitlab/embedded/service/gitlab-rails/config.ru:5:in `block in <main>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31341     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/rack-2.2.8/lib/rack/builder.rb:116:in `eval'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31341     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/rack-2.2.8/lib/rack/builder.rb:116:in `new_from_string'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31342     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/rack-2.2.8/lib/rack/builder.rb:105:in `load_file'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31342     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/rack-2.2.8/lib/rack/builder.rb:66:in `parse_file'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31343     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/puma-6.3.1/lib/puma/configuration.rb:366:in `load_rackup'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31343     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/puma-6.3.1/lib/puma/configuration.rb:288:in `app'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31344     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/puma-6.3.1/lib/puma/runner.rb:158:in `load_and_bind'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31344     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/puma-6.3.1/lib/puma/cluster.rb:359:in `run'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31344     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/puma-6.3.1/lib/puma/launcher.rb:194:in `run'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31345     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/puma-6.3.1/lib/puma/cli.rb:75:in `run'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31345     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/puma-6.3.1/bin/puma:10:in `<top (required)>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31346     from /opt/gitlab/embedded/bin/puma:25:in `load'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31346     from /opt/gitlab/embedded/bin/puma:25:in `<top (required)>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31347     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/cli/exec.rb:58:in `load'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31347     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/cli/exec.rb:58:in `kernel_load'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31348     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/cli/exec.rb:23:in `run'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31348     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/cli.rb:492:in `exec'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31349     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/vendor/thor/lib/thor/command.rb:27:in `run'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31350     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/vendor/thor/lib/thor/invocation.rb:127:in `invoke_command'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31350     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/vendor/thor/lib/thor.rb:392:in `dispatch'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31351     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/cli.rb:34:in `dispatch'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31351     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/vendor/thor/lib/thor/base.rb:485:in `start'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31352     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/cli.rb:28:in `start'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31352     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/bundler-2.4.19/exe/bundle:37:in `block in <top (required)>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31353     from /opt/gitlab/embedded/lib/ruby/site_ruby/3.0.0/bundler/friendly_errors.rb:117:in `with_friendly_errors'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31353     from /opt/gitlab/embedded/lib/ruby/gems/3.0.0/gems/bundler-2.4.19/exe/bundle:29:in `<top (required)>'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31354     from /opt/gitlab/embedded/bin/bundle:25:in `load'
tmpjsmx8vnu5o-gitlab-1  | 2023-09-25_20:09:11.31354     from /opt/gitlab/embedded/bin/bundle:25:in `<main>'
tmpjsmx8vnu5o-gitlab-1  |
```


#### Fixing it

This is needed for the other errors to occur

- `docker-compose exec gitlab bash`
- `mv /opt/gitlab/embedded/service/gitlab-rails/config/initializers/{7,5}_redis.rb`

### `ActionView::Template::Error (undefined method `first' for nil:NilClass):`

Try to create a new repository

#### Details

The full docker compose output related to this error:

```
tmpjsmx8vnu5o-gitlab-1  | ActionView::Template::Error (undefined method `first' for nil:NilClass):
tmpjsmx8vnu5o-gitlab-1  |     70:     %meta{ property: 'og:site_name', content: site_name }
tmpjsmx8vnu5o-gitlab-1  |     71:     %meta{ property: 'og:title', content: page_title }
tmpjsmx8vnu5o-gitlab-1  |     72:     %meta{ property: 'og:description', content: page_description }
tmpjsmx8vnu5o-gitlab-1  |     73:     %meta{ property: 'og:image', content: page_image }
tmpjsmx8vnu5o-gitlab-1  |     74:     %meta{ property: 'og:image:width', content: '64' }
tmpjsmx8vnu5o-gitlab-1  |     75:     %meta{ property: 'og:image:height', content: '64' }
tmpjsmx8vnu5o-gitlab-1  |     76:     %meta{ property: 'og:url', content: request.base_url + request.fullpath }
tmpjsmx8vnu5o-gitlab-1  |
tmpjsmx8vnu5o-gitlab-1  | app/models/application_setting_implementation.rb:487:in `pick_repository_storage'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/current_settings.rb:32:in `method_missing'
tmpjsmx8vnu5o-gitlab-1  | app/models/repository.rb:1215:in `pick_storage_shard'
tmpjsmx8vnu5o-gitlab-1  | app/models/project.rb:114:in `block in <class:Project>'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/gl_repository.rb:12:in `block in <class:GlRepository>'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/gl_repository/repo_type.rb:71:in `repository_for'
tmpjsmx8vnu5o-gitlab-1  | app/models/project.rb:1284:in `repository'
tmpjsmx8vnu5o-gitlab-1  | app/models/project.rb:1792:in `avatar_in_git'
tmpjsmx8vnu5o-gitlab-1  | app/models/project.rb:1796:in `avatar_url'
tmpjsmx8vnu5o-gitlab-1  | app/models/concerns/avatarable.rb:36:in `avatar_url'
tmpjsmx8vnu5o-gitlab-1  | app/helpers/page_layout_helper.rb:62:in `page_image'
tmpjsmx8vnu5o-gitlab-1  | app/views/layouts/_head.html.haml:73
tmpjsmx8vnu5o-gitlab-1  | app/views/layouts/application.html.haml:7
tmpjsmx8vnu5o-gitlab-1  | app/views/layouts/dashboard.html.haml:6
tmpjsmx8vnu5o-gitlab-1  | app/controllers/application_controller.rb:161:in `render'
tmpjsmx8vnu5o-gitlab-1  | app/controllers/application_controller.rb:520:in `set_current_admin'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/session.rb:11:in `with_session'
tmpjsmx8vnu5o-gitlab-1  | app/controllers/application_controller.rb:511:in `set_session_storage'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/i18n.rb:107:in `with_locale'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/i18n.rb:113:in `with_user_locale'
tmpjsmx8vnu5o-gitlab-1  | app/controllers/application_controller.rb:502:in `set_locale'
tmpjsmx8vnu5o-gitlab-1  | app/controllers/application_controller.rb:495:in `set_current_context'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/metrics/elasticsearch_rack_middleware.rb:16:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/memory_report.rb:13:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/speedscope.rb:13:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/database/load_balancing/rack_middleware.rb:23:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/rails_queue_duration.rb:33:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/etag_caching/middleware.rb:21:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/metrics/rack_middleware.rb:16:in `block in call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/metrics/web_transaction.rb:46:in `run'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/metrics/rack_middleware.rb:16:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/jira/middleware.rb:19:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/go.rb:20:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/query_analyzer.rb:11:in `block in call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/database/query_analyzer.rb:37:in `within'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/query_analyzer.rb:11:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/multipart.rb:173:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/read_only/controller.rb:50:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/read_only.rb:18:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/same_site_cookies.rb:27:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/basic_health_check.rb:25:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/handle_malformed_strings.rb:21:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/handle_ip_spoof_attack_error.rb:25:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/request_context.rb:15:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/webhook_recursion_detection.rb:15:in `call'
tmpjsmx8vnu5o-gitlab-1  | config/initializers/fix_local_cache_middleware.rb:11:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/compressed_json.rb:44:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/rack_multipart_tempfile_factory.rb:19:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/sidekiq_web_static.rb:20:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/metrics/requests_rack_middleware.rb:79:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/release_env.rb:13:in `call'
```

### `ActionView::Template::Error (First argument in form cannot contain nil or be empty):`

Try to access the general admin settings (`/admin/application_settings/general`)

#### Details

The full docker compose output related to this error:

```
tmpjsmx8vnu5o-gitlab-1  | ActionView::Template::Error (First argument in form cannot contain nil or be empty):
tmpjsmx8vnu5o-gitlab-1  |     1: = gitlab_ui_form_for @application_setting, url: general_admin_application_settings_path(anchor: 'js-visibility-settings'), html: { class: 'fieldset-form', id: 'visibility-settings' } do |f|
tmpjsmx8vnu5o-gitlab-1  |     2:   = form_errors(@application_setting)
tmpjsmx8vnu5o-gitlab-1  |     3:
tmpjsmx8vnu5o-gitlab-1  |     4:   %fieldset
tmpjsmx8vnu5o-gitlab-1  |
tmpjsmx8vnu5o-gitlab-1  | app/helpers/application_helper.rb:463:in `gitlab_ui_form_for'
tmpjsmx8vnu5o-gitlab-1  | app/views/admin/application_settings/_visibility_and_access.html.haml:1
tmpjsmx8vnu5o-gitlab-1  | app/views/admin/application_settings/general.html.haml:15
tmpjsmx8vnu5o-gitlab-1  | app/controllers/application_controller.rb:161:in `render'
tmpjsmx8vnu5o-gitlab-1  | app/controllers/application_controller.rb:520:in `set_current_admin'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/session.rb:11:in `with_session'
tmpjsmx8vnu5o-gitlab-1  | app/controllers/application_controller.rb:511:in `set_session_storage'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/i18n.rb:107:in `with_locale'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/i18n.rb:113:in `with_user_locale'
tmpjsmx8vnu5o-gitlab-1  | app/controllers/application_controller.rb:502:in `set_locale'
tmpjsmx8vnu5o-gitlab-1  | app/controllers/application_controller.rb:495:in `set_current_context'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/metrics/elasticsearch_rack_middleware.rb:16:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/memory_report.rb:13:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/speedscope.rb:13:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/database/load_balancing/rack_middleware.rb:23:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/rails_queue_duration.rb:33:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/etag_caching/middleware.rb:21:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/metrics/rack_middleware.rb:16:in `block in call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/metrics/web_transaction.rb:46:in `run'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/metrics/rack_middleware.rb:16:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/jira/middleware.rb:19:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/go.rb:20:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/query_analyzer.rb:11:in `block in call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/database/query_analyzer.rb:37:in `within'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/query_analyzer.rb:11:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/multipart.rb:173:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/read_only/controller.rb:50:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/read_only.rb:18:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/same_site_cookies.rb:27:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/basic_health_check.rb:25:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/handle_malformed_strings.rb:21:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/handle_ip_spoof_attack_error.rb:25:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/request_context.rb:15:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/webhook_recursion_detection.rb:15:in `call'
tmpjsmx8vnu5o-gitlab-1  | config/initializers/fix_local_cache_middleware.rb:11:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/compressed_json.rb:44:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/rack_multipart_tempfile_factory.rb:19:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/sidekiq_web_static.rb:20:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/metrics/requests_rack_middleware.rb:79:in `call'
tmpjsmx8vnu5o-gitlab-1  | lib/gitlab/middleware/release_env.rb:13:in `call'
```

## Notes

If there is a (proper) solution to any of these problems I will update this repository. See the thread [here](https://forum.gitlab.com/t/unable-to-set-up-gitlab-from-docker-container/93070)

**Update**: Yes, it was a configuration issue. Apperently the GitLab initialization fails, if the storages do not contain a storage, that is called "default"

